import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {
    private static StandardItem item1;

    @BeforeAll
    static void beforeAll() {
        item1 = new StandardItem(5, "testItem", 1, "TestCategory", 5);

    }
    @Test
    void testConstructor() {
        assertEquals(5, item1.getID());
        assertEquals("testItem", item1.getName());
        assertEquals(1, item1.getPrice());
        assertEquals("TestCategory", item1.getCategory());
        assertEquals(5, item1.getLoyaltyPoints());
    }

    @Test
    void testCopy() {
        StandardItem item1Copy = item1.copy();

        assertEquals(item1Copy.getID(), item1.getID());
        assertEquals(item1Copy.getName(), item1.getName());
        assertEquals(item1Copy.getPrice(), item1.getPrice());
        assertEquals(item1Copy.getCategory(), item1.getCategory());
        assertEquals(item1Copy.getLoyaltyPoints(), item1.getLoyaltyPoints());
    }

    @ParameterizedTest(name = "{0} equals {1} should be {2}")
    @CsvSource({
            "5, 5, true",
            "5, -5, false",
            "0, 0, true",
            "0, 10, false"
    })
    public void testEquals(int loyaltyPoints1, int loyaltyPoints2, boolean expectedResult) {
        StandardItem item2 = new StandardItem(5, "testItem", 1, "TestCategory", loyaltyPoints1);
        StandardItem item3 = new StandardItem(5, "testItem", 1, "TestCategory", loyaltyPoints2);
        boolean result = item2.equals(item3);

        Assertions.assertEquals(expectedResult, result);
    }
}