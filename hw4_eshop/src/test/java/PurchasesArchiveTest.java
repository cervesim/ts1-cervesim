import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;
import storage.ItemStock;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PurchasesArchiveTest {
    private static PurchasesArchive purchasesArchive;
    private static ItemPurchaseArchiveEntry ItPanMock;
    private static ArrayList<Order> orderArchiveMock;
    private static StandardItem standardItem1;
    private static StandardItem standardItem2;

    private static StandardItem standardItem3;
    private static ItemStock stock;
    @BeforeAll
    static void beforeAll() {
        purchasesArchive = new PurchasesArchive();
//        ItPanMock = Mockito.mock(ItemPurchaseArchiveEntry.class);
//        orderArchiveMock = Mockito.mock(ArrayList.class);

        ShoppingCart cart = new ShoppingCart();
        standardItem1 = new StandardItem(1,"itemName", 1,"categoryName", 1);
        standardItem2 = new StandardItem(2, "itemName", 2, "categoryName", 2);
        standardItem3 = new StandardItem(3, "itemName", 3, "categoryName", 3);
        cart.addItem(standardItem1);

        Order order = new Order(cart, "Jarmila Nechápu", "V zadnici Pac Totalne nic nesttiham", 1);
        purchasesArchive.putOrderToPurchasesArchive(order);


    }

    @Test
    void testPrintItemPurchaseStatistics() {

    }

    @Test
    void testGetHowManyTimesHasBeenItemSold() {
        Assertions.assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem1));
        Assertions.assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem2));
    }

    @Test
    void testPutOrderToPurchasesArchive() {
    }
    @Test
    void mockOrderArchive (){

    }
    @Test
    void mockItemPurchaseArchiveEntry(){
        ItemPurchaseArchiveEntry itemPurchaseEntry = Mockito.mock(ItemPurchaseArchiveEntry.class);

    }
}
