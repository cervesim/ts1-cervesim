import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;
import storage.ItemStock;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemStockTest {
    private static StandardItem standardItem1;
    private static ItemStock stock;

    @BeforeAll
    static void beforeAll() {
        standardItem1 = new StandardItem(1,"itemName", 55.55f,"categoryName", 1);
        stock = new ItemStock(standardItem1);
    }

    @ParameterizedTest
    @CsvSource({"1,1", "5,6", "10, 16", "-16, 0"})
    void increaseItemCountTest(int increaseBy, int expectedResult) {
        stock.IncreaseItemCount(increaseBy);
        assertEquals(expectedResult, stock.getCount());
    }

    @ParameterizedTest
    @CsvSource({"1,-1,", "5,-6", "10,-16", "-16, 0"})
    void decreaseItemCountTest(int decreaseBy, int expectedResult) {
        stock.decreaseItemCount(decreaseBy);
        assertEquals(expectedResult, stock.getCount());
    }
}
