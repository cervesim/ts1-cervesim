package storage;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import shop.DiscountedItem;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    private static ShoppingCart cart;
    private static ShoppingCart emptyCart;
    @BeforeAll
    static void beforeAll() {
        cart = new ShoppingCart();
        emptyCart = new ShoppingCart();
        StandardItem item1 = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        StandardItem item2 = new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10);
        StandardItem item3 = new StandardItem(3, "Screwdriver", 200, "TOOLS", 5);
        DiscountedItem item4 = new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013");
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);
        cart.addItem(item4);
    }

    @Test
    void testFirstOrderConstructor (){
        Order order1 = new Order(cart, "mistaJovaJova", "address, town, street", 5);
        Order order2 = new Order(emptyCart, null, null, 0);

        assertEquals(cart.getCartItems(), order1.getItems());
        assertEquals("mistaJovaJova", order1.getCustomerName());
        assertEquals("address, town, street", order1.getCustomerAddress());
        assertEquals(5, order1.getState());

        assertEquals(emptyCart.getCartItems(), order2.getItems());
        assertNull(order2.getCustomerName());
        assertNull(order2.getCustomerAddress());
        assertEquals(0, order2.getState());
    }
    @Test
    void testSecondOrderConstructor() {
        Order order1 = new Order(cart, "mistaJovaJova", "address, town, street");
        Order order2 = new Order(emptyCart, null, null);

        assertEquals(cart.getCartItems(), order1.getItems());
        assertEquals("mistaJovaJova", order1.getCustomerName());
        assertEquals("address, town, street", order1.getCustomerAddress());
        assertEquals(0, order1.getState());

        assertEquals(emptyCart.getCartItems(), order2.getItems());
        assertNull(order2.getCustomerName());
        assertNull(order2.getCustomerAddress());
        assertEquals(0, order2.getState());
    }
}