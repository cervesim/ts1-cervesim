package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class calculatorTest {

    //ARRANGE
    static calculator c;

    @BeforeAll
    public static void initVariable() {
        c = new calculator();
    }

    @Test
    public void addNumbers5And10_returnsNumber_15() {
        //ARRANGE
        calculator calculator = new calculator();

        //ACT
        int num = calculator.add(10, 5);

        //ASSERT
        Assertions.assertEquals(15, num);
    }

    @Test
    @Order(1)
    public void subtractNumbers5And10_returnsNumber_5() {
        //ACT
        int num = c.subtract(10, 5);

        //ASSERT
        Assertions.assertEquals(5, num);
    }

    @Test
    @Order(2)
    public void multiplyNumbers5And10_returnsNumber_50() {
        //ACT
        int num = c.multiply(5, 10);

        //ASSERT
        Assertions.assertEquals(50, num);
    }

    @Test
    public void divideNumbers5And10_returnsNumber_2() throws Exception {
        //ACT
        int num = c.divide(10, 5);
        Exception exception = Assertions.assertThrows(Exception.class, () -> c.divide(5, 0));

        //ASSERT
        Assertions.assertEquals(2, num);

        String expectedMessage = "Pokus o deleni nulou";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

}
